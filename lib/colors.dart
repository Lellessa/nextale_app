import 'package:flutter/material.dart';

class CustomColors {

  static const Color backgroundColor = Color(0xff303030);
  static const Color brown = Color(0xff1E1710);
  static const Color red = Color(0xffE5402D);

}