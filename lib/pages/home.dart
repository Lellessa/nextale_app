import 'package:page_transition/page_transition.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:nextale_app/colors.dart';
import 'package:nextale_app/pages/tale.dart';
import 'package:nextale_app/widgets/card.dart';


class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.backgroundColor,
      body: SafeArea(
        child: GridView.builder(
          itemCount: 12,
          padding: EdgeInsets.symmetric(vertical: 5, horizontal: 3),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            mainAxisSpacing: 8,
            crossAxisSpacing: 1,
            childAspectRatio: (2 / 3),
          ), 
          itemBuilder: (BuildContext context, int i) {
            if (i == 0) {
              // Chave responsável pelo state do flip card
              final GlobalKey<FlipCardState> cardKey = GlobalKey<FlipCardState>();

              return MainCard(
                image: 'assets/images/card_frente.png',
                cardKey: cardKey,

                // Animação de transição de página
                onTap: () => Navigator.push(context, PageTransition(type: PageTransitionType.rightToLeft, child: TalePage()))
                // onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context)=>TalePage()))
              );
            }

            return MainCard();
          }
        )
      ),
    );
  }
}