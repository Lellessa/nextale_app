import 'package:flutter/material.dart';
import 'package:nextale_app/colors.dart';
import 'package:nextale_app/texts.dart';
import 'package:nextale_app/widgets/parallax_image.dart';
import 'package:nextale_app/widgets/tale_container.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';

class TalePage extends StatefulWidget {

  @override
  _TalePageState createState() => _TalePageState();
}

class _TalePageState extends State<TalePage> {

  static AudioCache cache = AudioCache(prefix: 'assets/audios/');
  static AudioPlayer player;
  double volume = 1.0;
  bool first = true;

  void _play() async {
    player = await cache.loop('song.mp3');
  }

  void _stop() async {
    await player.stop();
  }

  void changeVol(double newVol) async {
    // Verificação de nulidade
    if (newVol == null)
      newVol = 1.0;

    if (player != null)
      await player.setVolume(newVol);
  }

  /* @override
  void initState() {
    _play();
    super.initState();
  } */

  @override
  void dispose() {
    _stop();
    
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    // Simulaçao do initState para remover erro de setState during build
    if (first) {
      _play();
      first = false;
    }

    return Scaffold(
      backgroundColor: CustomColors.brown,
      body: SafeArea(
        child: ParallaxImage(
          height: 350,
          image: AssetImage('assets/images/afrodite.gif'),
          function: changeVol,
          child: TaleContainer(
            backgroundColor: CustomColors.brown,
            tale: Tales().afroditeTale,
          ),
        ),
      ),
    );
  }
}