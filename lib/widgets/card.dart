import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';

class MainCard extends StatelessWidget {

  final String image;
  final Function onTap;
  final GlobalKey<FlipCardState> cardKey;

  MainCard({this.image = 'assets/images/card_verso.png', this.onTap, this.cardKey});

  @override
  Widget build(BuildContext context) {
    return FlipCard(
      key: this.cardKey,
      onFlipDone: (flipped) {
        if (this.cardKey != null && !this.cardKey.currentState.isFront) {
          // Desvirando o card da Afrodite
          this.cardKey.currentState.toggleCard();

          // Executando a função se for passada como parametro
          if (this.onTap != null) 
            this.onTap();
        }
          
      },
      front: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(this.image),
            fit: BoxFit.contain
          )
        ), 
      ),
      back: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/card_verso.png'),
            fit: BoxFit.contain
          )
        ), 
      ),

    );
    /* return GestureDetector(
      onTap: this.onTap,
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(this.image),
            fit: BoxFit.contain
          )
        ),
      ),
    ); */
  }
}