import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:nextale_app/colors.dart';
import 'package:nextale_app/widgets/animation_card.dart';

class ParallaxImage extends StatefulWidget {
  final ImageProvider image;
  final Widget child;
  final double height;
  final Function function;
  
  ParallaxImage({Key key, this.image, this.height, this.child, this.function}) : super(key: key);

  @override
  _ParallaxImageState createState() => _ParallaxImageState();
}

class _ParallaxImageState extends State<ParallaxImage> {

  ScrollController _scrollController;
  double _scrollPosition = 0;

  _scrollListener() {
    setState(() {
      _scrollPosition = _scrollController.position.pixels;
    });
  }


  double _getImageHeight() {
    // Imagem ocupando 90% da altura da tela
    double _height = MediaQuery.of(context).size.height * 0.9;

    if (_scrollPosition < _height) 
      return (_height - _scrollPosition);
    else
      return 0.0;
  }
  
  changeVol() {
    double vol;

    // Verificado se o scrollController está vinculado à algum scroolView
    if (_scrollController.hasClients) {
      double maxHeight = _scrollController.position.maxScrollExtent;
      double actualHeight = maxHeight - _scrollPosition;

      vol = (actualHeight) / maxHeight;
    } else {
      vol = 1;
    }

    // Verificação para som não ficar mudo
    if (vol < 0.1)
      vol = 0.1;

    // Chamando função para setar novo volume.
    widget.function(vol);
  }

  @override
  void initState() {

    // Instanciando o controller e adicionando o listener
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    changeVol();

    return SingleChildScrollView(
      controller: _scrollController,
      child: Stack(
        children: [

          Container(
            child: SingleChildScrollView(
              physics: NeverScrollableScrollPhysics(),
              child: AnimationCard(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.width*1.777,
              ),
            ),
          ),

          Container(
            margin: EdgeInsets.only(top: _getImageHeight()*0.90),
            child: widget.child,
          ),
          
        ],
      ),
    );
  }
}