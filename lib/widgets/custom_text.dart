import 'package:flutter/material.dart';

class CustomText extends StatelessWidget {

  final String text;
  final Color contrastColor, textColor;

  CustomText({this.text, this.contrastColor = Colors.red, this.textColor = Colors.white});

  @override
  Widget build(BuildContext context) {

    String firstCharacter = text[0].toUpperCase();
    String remainingText = text.substring(1);

    return RichText(
      text: TextSpan(
        text: firstCharacter,
        style: TextStyle(fontSize: 80, color: this.contrastColor, height: 0.2),
        children: <TextSpan>[
          TextSpan(text: remainingText, style: TextStyle(fontSize: 18, color: this.textColor, height: 1.5)),
        ],
      ),
    );
  }
}