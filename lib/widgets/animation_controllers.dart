import 'package:flutter/material.dart';
import 'package:nextale_app/colors.dart';
import 'package:nextale_app/widgets/animation_card.dart';

class MainAnimationController extends StatefulWidget {

  final MyGame game;
  MainAnimationController({this.game});

  @override
  _MainAnimationControllerState createState() => _MainAnimationControllerState();
}

class _MainAnimationControllerState extends State<MainAnimationController> {
  double stepTime = 0.04;

  @override
  Widget build(BuildContext context) {
    return  Container(
      margin: EdgeInsets.only(top: 20, right: 20),
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: CustomColors.red,
        borderRadius: BorderRadius.circular(200)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [

          Text('Step Time', style: TextStyle(color: Colors.white, fontSize: 10)),
          Text(stepTime.toStringAsFixed(2).toString(), style: TextStyle(color: Colors.white, fontSize: 18)),

          // Menos Velocidade
          SizedBox(height: 10),
          IconButton(
            icon: Icon(Icons.fast_rewind, size: 25, color: Colors.white,),
            onPressed: (){
              setState(() {
                stepTime += 0.01;
                widget.game.animation.animation.stepTime = stepTime;              
              });
            },
          ),

          // Pause
          SizedBox(height: 10),
          IconButton(
            icon: Icon(Icons.pause, size: 25, color: Colors.white,),
            onPressed: (){
              widget.game.pauseEngine();
            },
          ),

          // Resume
          SizedBox(height: 10),
          IconButton(
            icon: Icon(Icons.play_arrow, size: 25, color: Colors.white,),
            onPressed: (){
              widget.game.resumeEngine();
            },
          ),

          // Resume
          SizedBox(height: 10),
          IconButton(
            icon: Icon(Icons.repeat, size: 25, color: Colors.white,),
            onPressed: (){
              widget.game.animation.animation = widget.game.animation.animation.reversed();
            },
          ),

          // Mais Velocidade
          SizedBox(height: 10),
          IconButton(
            icon: Icon(Icons.fast_forward, size: 25, color: Colors.white,),
            onPressed: (){
              setState(() {
                if (stepTime > 0.01)
                  stepTime -= 0.01;
                widget.game.animation.animation.stepTime = stepTime;  
              });
            },
          ),

        ],
      ),
    );
  }
}