import 'package:flutter/material.dart';
import 'package:flame/flame.dart';
import 'package:flame/gestures.dart';
import 'package:flame/game.dart';
import 'package:flame/components.dart';
import 'package:nextale_app/colors.dart';
import 'package:nextale_app/widgets/animation_controllers.dart';

class AnimationCard extends StatefulWidget {
  
  final double width, height;
  AnimationCard({this.width, this.height});

  @override
  _AnimationCardState createState() => _AnimationCardState();
}

class _AnimationCardState extends State<AnimationCard> {
  bool first = true;

  MyGame game;

  @override
  void initState() {

    game = MyGame(screenHeight: widget.height, screenWidth: widget.width);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Stack(
      children: [

        // ANIMATION
        Container(
          width: widget.width,
          height: widget.height,
          child: GameWidget(game: game),
        ),

        // RECURSO TÉCNICO ALTERNATIVO PRA FAZER O LOADING
        /* (!game.isAttached) ? Container(
          width: widget.width,
          height: widget.height,
          padding: EdgeInsets.only(top: 200),
          alignment: Alignment.topCenter,
          color: CustomColors.backgroundColor,
          child: SizedBox(
            width: 40,
            height: 40,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
            ),
          )
        ) : Container(), */

        Align(
          alignment: Alignment.topRight,
          child: MainAnimationController(game: game,),
        ),

      ],
    );
  }
}



class MyGame extends BaseGame{

  final double screenWidth, screenHeight;
  MyGame({this.screenWidth, this.screenHeight});

  SpriteAnimationComponent animation = SpriteAnimationComponent();

  bool running = true;

  @override
  Future<void> onLoad() async {

    var spriteSheet = await images.load('spritesheet_reduced.png');
    final spriteSize = Vector2(this.screenWidth, this.screenHeight);
    SpriteAnimationData spriteData = SpriteAnimationData.sequenced(
      amount: 50, 
      stepTime: 0.04, 
      textureSize: Vector2(540.0, 960.0),
    );

    animation = SpriteAnimationComponent.fromFrameData(spriteSheet, spriteData)
      ..x = 0
      ..y = 0
      ..size = spriteSize;

    // animation.animation = animation.animation.reversed();
    add(animation);
  }

}