import 'package:flutter/material.dart';
import 'package:nextale_app/colors.dart';
import 'package:nextale_app/widgets/custom_text.dart';

class TaleContainer extends StatelessWidget {

  final String tale;
  final Color backgroundColor;

  TaleContainer({this.tale, this.backgroundColor = Colors.brown});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(30, 60, 30, 30),
      decoration: BoxDecoration(
        color: this.backgroundColor,
        borderRadius: BorderRadius.only(topLeft: Radius.circular(50), topRight: Radius.circular(50))
      ),
      child: CustomText(text: this.tale, contrastColor: CustomColors.red,),
    );
  }
}